import time
import csv
import argparse
import logging
import urllib

import multiprocessing as mp
from functools import partial

from typing import List

from datetime import datetime

from selenium import webdriver
from xvfbwrapper import Xvfb

from nedvizhimost import (
    get_offers, write_rows_from_list,
    show_contacts, go_to_next_page
)
from yandex import yandex_login
from helpers import read_config, go_to_page, timing, set_cookie, get_cookie

from mytypes import Credits, SearchSession, Category, ExtraData, Offer

logging.basicConfig(
    format=u'%(name)s # %(levelname)-8s [%(asctime)s]  %(message)s',
    level=logging.INFO
)

BASE_URL = 'https://realty.yandex.ru/management-new/search/'

GLOBAL_LOCK = None

COMMERCIAL: Category = {
    'types': [
        'OFFICE',
        'RETAIL',
        'FREE_PURPOSE',
        'WAREHOUSE',
        'MANUFACTURING',
        'LAND',
        'PUBLIC_CATERING',
        'AUTO_REPAIR',
        'HOTEL',
        'BUSINESS',
    ],
    'key': 'commercialType',
    'name': 'Коммерческая'
}

CATEGORIES = {
    "COMMERCIAL": COMMERCIAL
}

FIELDNAMES = list(Offer.__annotations__.keys())


def get_category_name(search: SearchSession) -> str:
    return CATEGORIES[search['category']]['name']


def get_offer_type(search: SearchSession) -> str:
    try:
        search['type']
        t = 'RENT'
    except KeyError:
        t = 'BUE'
    finally:
        return t


def crawler(browser: webdriver,
            writer: csv.DictWriter,
            search: SearchSession) -> int:
    ''' Ходит по страницам пока не упрется в последнюю '''
    type_ = get_offer_type(search)
    logger_name = f'[{search["category"]}][{search["commercialType"]}][{type_}]'
    logger = logging.getLogger(
        f'{logger_name} CRAWLER'
    )

    count_offers = 0
    current_page = 1

    offer_type = get_offer_type(search)
    offer_category = get_category_name(search)

    extra_data: ExtraData = {
        'offer_type': offer_type,
        'offer_category': offer_category
    }

    go: bool = True
    while go:
        time.sleep(1)
        logger.info(f'START CRAWLER FOR PAGE {current_page}')
        show_contacts(browser)
        offers_on_page = get_offers(browser, extra_data)
        if offers_on_page:
            count_offers += len(offers_on_page)
            write_rows_from_list(offers_on_page, writer, GLOBAL_LOCK)
        current_page += 1
        go = go_to_next_page(browser)  # Когда вернет False обход остановится.

    logger.info(f'CRAWLING IS FINISHED! COLLECT {count_offers} OFFERS')
    return count_offers


@timing
def runner(filename: str,
           search: SearchSession):

    type_ = get_offer_type(search)
    logger_name = f'[{search["category"]}][{search["commercialType"]}][{type_}]'
    logger = logging.getLogger(
        f'{logger_name} RUNNER'
    )

    url = f'{BASE_URL}?{urllib.parse.urlencode(search)}'  # type: ignore
    logger.info(f'URL {url}')

    file = open(filename, 'a')
    writer = csv.DictWriter(file, fieldnames=FIELDNAMES)

    try:
        with webdriver.Firefox() as browser:
            # STEP 1: Логинимся в яндексе используя куки которые сохранили на шаге 0 в main
            set_cookie(browser, ('https://yandex.ru/', 'Яндекс'))
            # STEP 2: Идем на страницу с объявлениями
            go_to_page(browser, url, 'Яндекс.Недвижимость')
            # STEP 4: Обходим все страницы
            crawler(browser, writer, search)
    finally:
        file.close()


def get_args():
    parser = argparse.ArgumentParser(description="Offers parser")
    parser.add_argument(
        '-F', '--filename',
        default=f'data/seans-{datetime.now().strftime("%d-%m-%Y-%H-%M-%S")}'
    )
    parser.add_argument('-L', '--login', default=None)
    parser.add_argument('-W', '--workers', type=int, default=4)
    parser.add_argument('-P', '--password', default=None)
    parser.add_argument('-C', '--config', default=None)
    parser.add_argument('-D', '--display', action='store_true')
    parser.add_argument(
        '-T', '--type',
        choices=['RENT', 'BUE', 'ALL'],
        default='RENT'
    )
    parser.add_argument(
        '-CA', '--category',
        type=str,
        choices=[
            'COMMERCIAL',
        ],
        default='COMMERCIAL'
    )
    parser.add_argument(
        '-CT', '--categorytype',
        type=str,
        default='ALL'
    )

    args = parser.parse_args()
    return args


def get_search_parameters(args) -> List:
    search_sessions = []
    type_ = args.type  # rent, bue, all
    category = args.category

    for cat in CATEGORIES[category]['types']:
        search: SearchSession = {
            'category': category,
            CATEGORIES[category]['key']: cat  # type: ignore
        }

        if type_ == 'ALL':
            search_with_rent: SearchSession = {
                'category': category,
                CATEGORIES[category]['key']: cat,  # type: ignore
                'type': 'RENT'
            }
            search_sessions.append(search_with_rent)

        if type_ == 'RENT':
            search['type'] = 'RENT'

        search_sessions.append(search)
    return search_sessions


def get_credits_from_args(args):
    login = args.login
    password = args.password
    config = args.config

    if not any([config, all([password, login])]):
        logging.warning('Please give me login/password or configfile!')
        exit(0)

    if all([config, password, login]):
        logging.warning('Please chuse one method: login/password or configfile!')
        exit(0)

    if any([config, all([password, login])]):
        credits: Credits = read_config(config) if config else {
            'login': login,
            'password': password
        }
        return credits


@timing
def main():
    args = get_args()

    filename = args.filename + '.csv'
    workers = args.workers

    credits = get_credits_from_args(args)

    if not args.display:
        vdisplay = Xvfb()
        vdisplay.start()

    global GLOBAL_LOCK
    GLOBAL_LOCK = mp.Lock()

    # STEP 0: Логинимся в Яндексе, сохраняем куки для дальнейшего использования
    with webdriver.Firefox() as browser:
        yandex_login(browser, credits)
        get_cookie(browser, ('https://yandex.ru', 'Яндекс'))

    file = open(filename, 'a')
    writer = csv.DictWriter(file, fieldnames=FIELDNAMES)
    writer.writeheader()
    file.close()

    runner_prepared = partial(runner, filename)

    search_parameters = get_search_parameters(args)

    try:
        pool = mp.Pool(workers)
        pool.map(runner_prepared, search_parameters)
    except Exception:
        logging.exception('THERE IS AN EXCEPTION WHILE RUNNING MAIN')
    finally:
        pool.close()
        pool.join()
        vdisplay.stop() if not args.display else None


if __name__ == "__main__":
    main()
