import unittest
from unittest import TestCase

from datetime import datetime

from helpers import get_int_from_str, check_date

class TestStrToIntCoverter(TestCase):
    def test_get_int_from_str_return_int(self):
        '''Входящая строка целое число'''
        test_string = '85 000 000 ₽'
        integer = get_int_from_str(test_string)

        self.assertEqual(type(integer), int)
        self.assertEqual(85000000, integer)

    def test_get_int_from_str_return_float(self):
        '''Входящая строка с разделителем точкой'''
        test_string = '85 000 000.546 ₽'
        float_ = get_int_from_str(test_string)

        self.assertEqual(type(float_), float)
        self.assertEqual(85000000.546, float_)

    def test_get_int_from_str_return_int_from_squere(self):
        '''Входящая строка целое число со спецсимволом'''
        test_integer_str = '1116 м²'
        integer = get_int_from_str(test_integer_str)

        self.assertEqual(type(integer), int)
        self.assertEqual(1116, integer)

    def test_get_int_from_str_return_float_from_squere(self):
        '''Входящая строка целое число со спецсимволом и разделиетелем запятой'''
        test_float_str = '2626,8 м²'
        float_ = get_int_from_str(test_float_str)

        self.assertEqual(type(float_), float)
        self.assertEqual(2626.8, float_)


class TestCheckDate(TestCase):
    def test_check_date_return_correct_value_if_wrong_date(self):
        wrong_date = '18 марта '
        date = check_date(wrong_date)
        check = f'18 марта {datetime.now().year}'
        self.assertEqual(date, check)

    def test_check_date_return_correct_value_if_correct_date(self):
        correct_date = '18 марта 2019'
        date = check_date(correct_date)
        self.assertEqual(date, correct_date)


if __name__ == "__main__":
    unittest.main()
