import unittest
from unittest import TestCase

from main import (
    get_search_parameters,
    # get_category_name,
    # get_offer_type,
    CATEGORIES,
)


class MockArgs:
    pass


class TestGetOfferType(TestCase):
    def test_get_offer_type(self):
        pass


class TestGetCategoryName(TestCase):
    def test_get_category_name(self):
        pass


class TestGetSearchParametersRent(TestCase):
    def setUp(self):
        self.args_rent = MockArgs()
        self.args_rent.type = 'RENT'
        self.args_rent.category = 'COMMERCIAL'

        self.args_all = MockArgs()
        self.args_all.type = 'ALL'
        self.args_all.category = 'COMMERCIAL'

        self.args_bue = MockArgs()
        self.args_bue.type = 'BUE'
        self.args_bue.category = 'COMMERCIAL'

    def test_get_search_parameters_rent(self):
        params = get_search_parameters(self.args_rent)
        self.assertEqual(10, len(params))
        for p in params:
            self.assertEqual(p['category'], 'COMMERCIAL')
            self.assertIn(p['commercialType'], CATEGORIES['COMMERCIAL']['types'])
            self.assertEqual(p['type'], 'RENT')

    def test_get_search_parameters_all(self):
        params = get_search_parameters(self.args_all)
        self.assertEqual(20, len(params))

        for p in params[0::2]:
            self.assertEqual(p['category'], 'COMMERCIAL')
            self.assertIn(p['commercialType'], CATEGORIES['COMMERCIAL']['types'])
            self.assertEqual(p['type'], 'RENT')

        for p in params[1::2]:
            self.assertEqual(p['category'], 'COMMERCIAL')
            self.assertIn(p['commercialType'], CATEGORIES['COMMERCIAL']['types'])
            with self.assertRaises(KeyError):
                p['type']

    def test_get_search_parameters_bue(self):
        params = get_search_parameters(self.args_bue)
        self.assertEqual(10, len(params))
        for p in params:
            self.assertEqual(p['category'], 'COMMERCIAL')
            self.assertIn(p['commercialType'], CATEGORIES['COMMERCIAL']['types'])
            with self.assertRaises(KeyError):
                p['type']


if __name__ == "__main__":
    unittest.main()
