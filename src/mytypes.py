from typing import TypedDict, Optional, List


class ExtraData(TypedDict):
    offer_type: str
    offer_category: str


class Category(TypedDict):
    types: List[str]
    key: str
    name: str


class Credits(TypedDict):
    login: str
    password: str


class Offer(TypedDict):
    offer_category: str  # Коммерческая недвижимость / квартриры и e.t.c
    offer_type: str  # аренда/покупка
    offer_name: str  # офис/квартирр e.t.c.
    offer_link: str
    owner_type: str  # агентство/владелец e.t.c.
    owner_name: str
    owner_phone: str
    offer_address: str
    offer_metro: Optional[str]
    offer_area: float
    offer_price: float
    offer_price_for_m: float  # цена за м*м
    offer_date: str


class SearchSession(TypedDict):
    type: Optional[str]
    category: str
    commercialType: Optional[str]
