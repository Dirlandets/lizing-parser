import time
import logging
import asyncio
import configparser
import pickle
import re

from datetime import datetime
from functools import wraps

from typing import Union, Callable, Tuple

from selenium import webdriver
import selenium.common.exceptions as sex  # ulala

from mytypes import Credits

MAX_WAIT = 10


def read_config(configfile_path: str) -> Credits:
    config = configparser.ConfigParser()
    config.read(configfile_path)
    credits: Credits = {
        'login': config['CREDITS']['login'],
        'password': config['CREDITS']['password']
    }
    return credits


def wait(fn):
    def modified_fn(*args, **kwargs):
        start_time = time.time()
        while True:
            try:
                return fn(*args, **kwargs)
            except (AssertionError, sex.WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)
    return modified_fn


@wait
def go_to_page(browser: webdriver, url: str, title: str, delay: Union[float, int] = 1):
    logger = logging.getLogger('PAGE LOADER')

    logger.info(f'GET PAGE {url}')
    browser.get(url)
    time.sleep(delay)
    assert title in browser.title
    logger.info(f'SUCCESSFULY GET PAGE {url}')


def timing(f: Callable):
    logger = logging.getLogger('TIMER')
    is_async = asyncio.iscoroutinefunction(f)
    if is_async:
        @wraps(f)
        async def wraped(*args, **kw):
            ts = time.perf_counter()
            logger.info(f'func:{f.__name__} START {ts}')
            result = await f(*args, **kw)
            te = time.perf_counter()
            logger.info(f'func: {f.__name__} TOOK {te-ts} sec')
            return result

    else:
        @wraps(f)
        def wraped(*args, **kw):
            ts = time.perf_counter()
            result = f(*args, **kw)
            te = time.perf_counter()

            print(
                f'func:{f.__name__}  took: {te-ts} sec'
            )
            return result
    return wraped


def get_cookie(browser: webdriver, coockie_target: Tuple[str, str]):
    logger = logging.getLogger('COOKIE SAVER')
    go_to_page(browser, coockie_target[0], coockie_target[1])
    try:
        pickle.dump(browser.get_cookies(), open("cookies.pkl", "wb"))
        logger.info(f'COOKIE FOR {browser.current_url} SUCCESFULY SAVED')
    except Exception:
        logger.exception(f'ERROR WHILE GETTING COOKIE FOR {browser.current_url}')


def set_cookie(browser: webdriver, cookie_target: Tuple[str, str]):
    logger = logging.getLogger('COOKIE SETTER')
    go_to_page(browser, cookie_target[0], cookie_target[1])
    try:
        cookies = pickle.load(open("cookies.pkl", "rb"))
        for cookie in cookies:
            browser.add_cookie(cookie)
        logger.info(f'COOKIE FOR {browser.current_url} SUCCESFULY SET')
    except Exception:
        logger.exception(f'ERROR WHILE SETTING COOKIE FOR {browser.current_url}')


def get_int_from_str(string: str) -> float:
    result = []
    is_float = False
    delimeters = [',', '.']
    for i in string:
        if i in delimeters:
            i = '.'
            result.append(i)
            is_float = True
        else:
            try:
                int(i)
                result.append(i)
            except ValueError:
                continue

    joined_result = ''.join(result)

    if is_float:
        return_value = float(joined_result)
    else:
        return_value = int(joined_result)

    return return_value


R = re.compile(r'\d*\s\w+$')


def check_date(date_string: str) -> str:
    striped_string = date_string.strip()
    match = R.match(striped_string)
    if match:
        year = datetime.now().year
        return f'{striped_string} {year}'
    else:
        return striped_string
