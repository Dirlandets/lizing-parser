import time
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from helpers import Credits, go_to_page

import logging

logger = logging.getLogger('LOGIN')


def yandex_login(browser: webdriver, credits: Credits) -> None:
    # STEP 1: Зайти на Яндекс и залогиниться
    logger.info('START LOGIN TO YANDEX')

    go_to_page(browser, 'https://yandex.ru', 'Яндекс')
    login_button = browser.find_element_by_xpath('/html/body/div[1]/div[1]/div/div[1]/div/a[1]')
    login_button.click()

    # STEP 2: Логинимся
    assert 'Авторизация' in browser.title
    login_input = browser.find_element_by_id('passp-field-login')
    login_input.send_keys(credits['login'])
    time.sleep(0.4)
    login_input.send_keys(Keys.RETURN)
    time.sleep(0.5)
    pass_input = browser.find_element_by_id('passp-field-passwd')
    pass_input.send_keys(credits['password'])
    time.sleep(0.2)
    pass_input.send_keys(Keys.RETURN)
    time.sleep(10)
    assert 'Входящие — Яндекс.Почта' in browser.title
    logger.info('SUCCESSFULY LOGED IN')
