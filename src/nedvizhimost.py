import time
import csv
import logging

from typing import List

from selenium import webdriver
import selenium.common.exceptions as sex  # ulala

from mytypes import Offer, ExtraData

from helpers import get_int_from_str, check_date


def check_popup(browser: webdriver):
    try:
        popup_close = browser.find_element_by_css_selector('.subscription-wizard__close-button')
        popup_close.click()
        time.sleep(1)
        return True
    except sex.ElementNotInteractableException:
        return False


def get_offers(browser: webdriver, extra_data: ExtraData) -> List[Offer]:
    logger = logging.getLogger('GET OFFERS')
    offers = browser.find_elements_by_css_selector('div.profsearch-offer')
    offers_list: List[Offer] = []
    if offers:

        for n, offer in enumerate(offers):
            logger.debug(f'COLLECT DATA FOR OFFER {n}')

            offer_name_element = offer.find_element_by_css_selector('.profsearch-offer-name')
            offer_a = offer_name_element.find_element_by_css_selector('a.profsearch-offer-name__name')
            offer_link = offer_a.get_attribute("href")
            offer_name = offer_a.text

            try:
                offer_area_element = offer.find_element_by_css_selector('div.profsearch-offer-area__area')
                offer_area = get_int_from_str(offer_area_element.text)
            except sex.NoSuchElementException:
                offer_area = 0.0

            offer_price_element = offer.find_element_by_css_selector('div.profsearch-offer-price__price')
            offer_price = get_int_from_str(offer_price_element.text)

            offer_address_element = offer.find_element_by_css_selector('div.profsearch-offer-address')

            offer_address = offer_address_element.find_element_by_css_selector(
                'a.profsearch-offer-address__address'
            ).text

            offer_date = offer.find_element_by_css_selector('div.profsearch-offer-date__date').text

            try:
                offer_metro = offer_address_element.find_element_by_css_selector(
                    'div.profsearch-offer-address__metro'
                ).text
            except sex.NoSuchElementException:
                offer_metro = None

            offer_owner = offer.find_element_by_css_selector('div.profsearch-offer-owner__content')

            owner_details = offer_owner.find_elements_by_class_name('profsearch-offer-owner__name')
            try:
                owner_type, owner_name = [string.text for string in owner_details]
            except ValueError:
                owner_type, owner_name = owner_details[0].text, None
            owner_phone = offer_owner.find_element_by_css_selector('span.profsearch-offer-owner__phone-text').text

            offer_dict: Offer = {
                'offer_type': extra_data['offer_type'],
                'offer_category': extra_data['offer_category'],
                'offer_name': offer_name,
                'offer_link': offer_link,
                'owner_type': owner_type,
                'owner_name': owner_name,
                'owner_phone': owner_phone,
                'offer_area': float(offer_area),
                'offer_price': float(offer_price),
                'offer_price_for_m': offer_price / offer_area if offer_area else 0.0,
                'offer_address': offer_address,
                'offer_metro': offer_metro,
                'offer_date': check_date(offer_date)
            }

            offers_list.append(offer_dict)
            logger.debug(f'DATA FOR OFFER {n} COLLECTED')
    return offers_list


def show_contacts(browser: webdriver) -> None:
    logger = logging.getLogger('SHOW CONTACTS')

    show_contacts_button = browser.find_elements_by_css_selector('button.profsearch-cluster__toggle')
    if show_contacts_button:
        prev_pos: int = 0
        for n, button in enumerate(show_contacts_button):
            browser.execute_script(f'window.scrollTo({prev_pos}, {button.location["y"]-60} )')
            prev_pos = button.location["y"]
            logger.debug(f'TOUGLE CONTACT {n}')
            button.click()
            time.sleep(0.1)


def check_loading(browser: webdriver) -> bool:
    logger = logging.getLogger('PAGELOAD')
    try:
        logger.debug('LOAD PAGE')
        browser.find_element_by_class_name('profsearch-table__spinner')
        return check_loading(browser)
    except sex.NoSuchElementException:
        logger.debug('PAGE LOADED')
        return True


def go_to_next_page(browser: webdriver):
    paginator_links = browser.find_elements_by_class_name('Pager__radio-link')
    try:
        button = [link for link in paginator_links if link.text == 'Следующая'][0]
        browser.execute_script(f'window.scrollTo({0}, {button.location["y"]-60})')
        button.click()
        check_loading(browser)
        return True
    except IndexError:
        return False


def write_rows_from_list(items: List[Offer],
                         writer: csv.DictWriter, lock) -> None:
    for item in items:
        lock.acquire()
        try:
            writer.writerow(item)
        finally:
            lock.release()
