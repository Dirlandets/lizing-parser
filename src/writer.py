import csv


def write_row(data: dict,
              writer: csv.DictWriter,
              worker_id: int = None):
    if data:
        writer.writerow(data)
